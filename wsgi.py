from flask import Flask, request, json, jsonify, abort, Response
from flask_cors import CORS
# from pprint import pprint

# from flasgger import Swagger
# from apispec import APISpec
# from apispec.ext.marshmallow import MarshmallowPlugin
# from apispec_webframeworks.flask import FlaskPlugin
# from marshmallow import Schema, fields

import sqlalchemy as db
# import urllib3
import cx_Oracle
# import models
import os

application = Flask(__name__)
# application.config['SWAGGER'] = {
#     "title": "Python Flask Backend API",
#     "uiversion" : 3,
# }

# # Create an APISpec
# spec = APISpec(
#     title="Swagger Petstore",
#     version="1.0.0",
#     openapi_version="3.0.2",
#     info=dict(description="test API"),
#     plugins=[FlaskPlugin(), MarshmallowPlugin()],
#     schema_name_resolver=None,
# )


# class UsersSchema(Schema):
#     id = fields.Int()
#     username = fields.Str(required=True)
#     email = fields.Str(required=True)

# swag = Swagger(app=application, template={
#     "openapi": "3.0.2",
#     "info": {
#         "title": "Example application for Python Flask",
#         "version": "0.1"
#     }
# })


engine = db.create_engine("oracle+cx_oracle://logbook_ti_dev:"+ os.environ['databasepassword'] + "@devdb11-s.cern.ch:10121/DEVDB112")

# Enables CORS
CORS(application)

@application.route('/users', methods=['GET', 'POST'])
def users():
    """Get all users from system
    ---
    get:
        description: Get all users from system
        responses:
            200:
              content:
                  application/json:
                    schema: UsersSchema
    post:
        description: Create new user
        responses:
            200:
              content:
                application/json:
                    schema: UsersSchema
    """
    if request.method == 'GET':
        try:
            conn = engine.connect()
            query = 'SELECT * from USER_TEST'
            ResultProxy = conn.execute(query)
            ResultSet = ResultProxy.fetchall()
            usersList = []
            for users in ResultSet:
                user = {
                    "ID": users[0],
                    "USERNAME": users[1],
                    "EMAIL": users[2]
                }
                usersList.append(user)
            return json.dumps(usersList)
        except Exception as e:
            print('Problem when getting users from database: ' + str(e))
            return 
    elif request.method == 'POST':
        try:
            newUsername = request.json['USERNAME']
            newEmail = request.json['EMAIL']
            response = ''
            if not newUsername and not newEmail: 
                raise Exception('Username or email not provided')
            if newUsername and newEmail:
                conn = engine.connect()
                query = 'INSERT INTO USER_TEST (USERNAME, EMAIL) VALUES (:newUsername, :newEmail)'
                result = conn.execute(query, [newUsername, newEmail])
                conn.close()
                return jsonify(success=True)
        except Exception as e:
            print('Problem when posting new user to database: ' + str(e))

@application.route('/users/<int:id>', methods=['GET', 'PUT', 'DELETE'])
def user(id):
    if request.method == 'GET':
        try:
            conn = engine.connect()
            query = 'SELECT * FROM USER_TEST WHERE ID = :id'
            result = conn.execute(query, id)
            ResultProxy = result.fetchone() # Returns record as a tuple
            if ResultProxy:
                user = {
                    "ID": ResultProxy[0],
                    "USERNAME": ResultProxy[1],
                    "EMAIL": ResultProxy[2]
                }
                conn.close()
                return json.dumps(user)
            else: return abort(Response('User could not be found'))
        except Exception as e:
            print('Problem when getting specific user from database: ' + str(e))
            return abort(e)
        return 
    elif request.method == 'PUT':
        try: 
            conn = engine.connect()
            print(request.data)
            newUsername = request.json['USERNAME']
            newEmail = request.json['EMAIL']
            query = 'UPDATE USER_TEST SET USERNAME = :newUsername, EMAIL = :email WHERE ID = :id'
            result = conn.execute(query, [newUsername, newEmail, id])
            conn.close()
            return jsonify(success=True)
        except Exception as e:
            print('Problem when updating user in database: ' + str(e))
            return abort(e)

    elif request.method == 'DELETE':
        try:
            conn = engine.connect()
            query = 'DELETE FROM USER_TEST WHERE ID = :id'
            result = conn.execute(query, id)
            conn.close()
            return jsonify(success=True)
        except Exception as e:
            print('Problem when deleting user from database: ' + str(e))
            return abort(e)



if __name__ == '__main__':
    application.run(host='0.0.0.0', port=8080)
