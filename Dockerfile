# FROM centos:7
FROM gitlab-registry.cern.ch/linuxsupport/cc7-base

RUN yum install -y \
    python36 \
    wget gcc make \
    bzip2-devel zlib-devel epel-release python36-pip libaio 
    
# Install the Oracle instant client.
RUN yum -y --enablerepo cernonly install oracle-instantclient11.2-basiclite oracle-instantclient11.2-meta oracle-instantclient-tnsnames.ora.noarch

RUN mkdir /opt/Flaskapp
RUN cd /opt/Flaskapp
COPY *.py /opt/Flaskapp/
COPY requirements.txt /opt/Flaskapp/requirements.txt
# Install the requirements for running the application
RUN pip3.6 install --trusted-host pypi.python.org -r /opt/Flaskapp/requirements.txt

EXPOSE 8080/tcp

WORKDIR /opt/Flaskapp

ENTRYPOINT ["python36","wsgi.py"]