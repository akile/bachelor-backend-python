from ldap3 import Server, Connection, ALL, ALL_ATTRIBUTES

def person():    
    username = request.headers['X-Remote-User']
    print(request.headers)

    server = Server("xldap.cern.ch")
    base_dn = "OU=Users,OU=Organic Units,DC=cern,DC=ch"
    conn = Connection(server, auto_bind=True)
    search_filter = "(CN=" + username + ")"
    result = conn.search(search_base=base_dn, search_filter=search_filter, attributes=ALL_ATTRIBUTES)
    print(conn.entries[0])

